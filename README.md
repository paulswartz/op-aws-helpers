# 1Password AWS Helpers

## op-aws-credential-helper

Configures the AWS CLI to use an IAM access key stored in 1Password.

This is a lightly-modified version of [claui/aws-credential-1password](https://github.com/claui/aws-credential-1password), 
released under the [Apache License 2.0](https://github.com/claui/aws-credential-1password/blob/main/LICENSE).

Usage:

``` text
# in $HOME/.aws/config
[profile XXX]
credential_process = op-aws-credential-helper "Vault Name" "Item Name" "access key field label" "secret key field label"
```

If you have multiple vaults, you can use `env` to provide the `OP_ACCOUNT` environment variable:

```text
[profile XXX]
credential_process = env OP_ACCOUNT=team-name.1password.com op-aws-credential-helper Vault Item "access key field label" "secret key field label"
```

Depends on having the [1Password CLI][op-cli] installed.

## op-aws-rotate

Automatically rotates the AWS IAM access key stored in 1Password. Requires that
you use `op-aws-credential-helper` or `aws-credential-1password` for the given
profile.

Usage:

``` sh
$ op-aws-rotate --profile XXX
Deactivating AKIAWKDUC67OSRWFID5C...
Created AKIAWKDUC67OTD2E6U7Y
Disabling AKIAWKDUC67OVFPRECOA...
Done!
```

If you're using the [1Password AWS Plugin][op-aws-plugin], you can give the path
to a directory configured to use the plugin as an option for rotation:

``` sh
$ op-aws-rotate --cli-plugin-path <path>
Deactivating AKIAWKDUC67OSRWFID5C...
Created AKIAWKDUC67OTD2E6U7Y
Disabling AKIAWKDUC67OVFPRECOA...
Done!
```

Depends on having [Python 3][py3], the [1Password CLI][op-cli], and the [AWS CLI][aws-cli] installed.

## Usage via Nix Flakes
Must have the [Nix Package Manager](https://nixos.org/) installed and have the experimental features `flakes` and `nix-command` enabled.

```
<in /etc/nix/nix.conf>
experimental-features = flakes nix-command
```

All the following `nix` commands need to be run with `NIXPKGS_ALLOW_UNFREE=1` and `--impure` because the license on the 1Password-cli dependency is unfree.

replace `gitlab:paulswartz/op-aws-helpers#` with `.#` if running from a local checkout.

### Building `op-aws-rotate`
```sh
: NIXPKGS_ALLOW_UNFREE=1 nix build 'gitlab:paulswartz/op-aws-helpers#op-aws-rotate' --impure
```

### Running `op-aws-rotate`
Running from nix is equivalent to installing and the script and installing the dependencies.
```sh
: NIXPKGS_ALLOW_UNFREE=1 nix run 'gitlab:paulswartz/op-aws-helpers#op-aws-rotate' --impure -- <args>
```
Refer to [op-aws-rotate instructions](#op-aws-rotate) for usage and `<args>`.

[py3]: https://www.python.org/downloads/
[op-cli]: https://developer.1password.com/docs/cli/
[op-aws-plugin]: https://developer.1password.com/docs/cli/shell-plugins/aws
[aws-cli]: https://aws.amazon.com/cli/
