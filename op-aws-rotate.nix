{ lib
, runCommand
, makeWrapper
, python3
, _1password
, awscli2
, ...
}: runCommand "op-aws-rotate" { nativeBuildInputs = [ makeWrapper ]; } (lib.concatLines [
	"mkdir -p $out/bin"
	"cp ${./op-aws-rotate} $out/bin/op-aws-rotate"
	"patchShebangs --host $out/bin/op-aws-rotate"
	"wrapProgram $out/bin/op-aws-rotate \\"
		"--prefix PATH : ${lib.makeBinPath [
			_1password
			awscli2
		]}"
])
