#!/usr/bin/env python3
import importlib
from pathlib import Path
import sys
import unittest

true_globals = globals()
exec_globals = true_globals.copy()
exec_globals["__name__"] = "op-aws-rotate"
op_aws_rotate_path = (Path(__file__).parent.parent /
                      "op-aws-rotate")
exec(op_aws_rotate_path.open().read(), exec_globals)

for (name, value) in exec_globals.items():
    if name not in true_globals:
        true_globals[name] = value


class ParseCredentialHelperTest(unittest.TestCase):
    def test_basic(self):
        helper = "op-aws-credential-helper vault item access_key secret_key"
        expected = {'vault': 'vault', 'item': 'item', 'access_key_field': 'access_key',
                    'secret_key_field': 'secret_key', 'account': None}
        actual = parse_credential_helper(helper)
        self.assertEqual(actual, expected)

    def test_with_account(self):
        helper = "env OP_ACCOUNT=account op-aws-credential-helper vault item access_key secret_key"
        expected = {'vault': 'vault', 'item': 'item', 'access_key_field': 'access_key',
                    'secret_key_field': 'secret_key', 'account': 'account'}
        actual = parse_credential_helper(helper)
        self.assertEqual(actual, expected)

    def test_different_path(self):
        helper = "/home/user/bin/op-aws-credential-helper vault item access_key secret_key"
        expected = {'vault': 'vault', 'item': 'item', 'access_key_field': 'access_key',
                    'secret_key_field': 'secret_key', 'account': None}
        actual = parse_credential_helper(helper)
        self.assertEqual(actual, expected)

    def test_cannot_find(self):
        helper = "different-command"
        self.assertRaises(ValueError, lambda: parse_credential_helper(helper))


if __name__ == "__main__":
    unittest.main()
